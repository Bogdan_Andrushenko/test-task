﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceManager : MonoBehaviour
{
    public event Action OnInterfaceRestartClick;
    public event Action OnInterfaceExitClick;
    public event Action OnInterfaceStartClick;

    [SerializeField]
    private Text score;
    [SerializeField]
    private Text ghostCount;
    [SerializeField]
    private Button MainMenu;
    [SerializeField]
    private Button StartGame;
    [SerializeField]
    private Button Exit;
    [SerializeField]
    private Button RestartGame;

    private bool inMainMenu = true;
    private bool SideMenuActive = false;

    public void GameStart()
    {
        TogleMainMenu();
        OnInterfaceStartClick.Invoke();
    }

    public void MainManu()
    {
        TogleSideMenu();
    }

    public void GameExit()
    {
        OnInterfaceExitClick.Invoke();
        TogleMainMenu();
        RestartGame.gameObject.SetActive(false);
        Exit.gameObject.SetActive(false);
        StartGame.gameObject.SetActive(true); ;
    }

    public void Restart()
    {
        OnInterfaceRestartClick();
        Exit.gameObject.SetActive(false);
        RestartGame.gameObject.SetActive(false);
    }


    public void UpdateUserData(int Score, int Ghost)
    {
        ghostCount.text = String.Format("{0}/10", Ghost);
        score.text = Score.ToString();
    }

    private void TogleMainMenu()
    {
        inMainMenu = !inMainMenu;
        SideMenuActive = !SideMenuActive;
        StartGame.gameObject.SetActive(false);
        MainMenu.gameObject.SetActive(!inMainMenu);
        score.gameObject.SetActive(!inMainMenu);
        ghostCount.gameObject.SetActive(!inMainMenu);
    }

    public void TogleSideMenu()
    {
        Exit.gameObject.SetActive(SideMenuActive);
        RestartGame.gameObject.SetActive(SideMenuActive);
    }


}
