﻿using UnityEngine;


public class Ghost : MonoBehaviour
{

    [SerializeField]
    private int max = 6;
    [SerializeField]
    private int min = 1;

    private const int HeightToDestroy = 18;

    void Start()
    {
        var r = GetComponent<Rigidbody>();
        r.velocity = Vector2.up * Random.Range(min, max);
    }

    private void Update()
    {
        if (transform.position.y > 18)
        {
            Destroy(gameObject);
        }
    }


}
