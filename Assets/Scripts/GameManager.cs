﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private const float SpawnStep = 5;
    private const int ScoreForGhost = 5;

    private readonly List<GameObject> ghostList = new List<GameObject>();

    [SerializeField] private GameObject ghostprefab;

    [SerializeField] private InputManager inputManager;

    [SerializeField] private InterfaceManager interfaceManager;

    private int score;
    private int ghostCheck;

    private void Start()
    {
        inputManager.OnGhostClickedevent += InputManager_OnGhostClickedevent;
        interfaceManager.OnInterfaceStartClick += InterfaceManager_OnInterfaceStartClick;
        interfaceManager.OnInterfaceRestartClick += InterfaceManager_OnInterfaceRestartClick;
        interfaceManager.OnInterfaceExitClick += InterfaceManager_OnInterfaceExitClick;
    }

    private void InterfaceManager_OnInterfaceExitClick()
    {
        score = 0;
        inputManager.gameObject.SetActive(false);
        foreach (var g in ghostList)
        {
            Destroy(g);
        }

        ghostList.Clear();

    }

    private void InterfaceManager_OnInterfaceRestartClick()
    {

        score = 0;
        InterfaceManager_OnInterfaceExitClick();
        InterfaceManager_OnInterfaceStartClick();

    }

    private void InterfaceManager_OnInterfaceStartClick()
    {

        for (var i = 0; i < 10; i++)
        {
            var p = new Vector3(-20 + i * SpawnStep + Random.Range(-SpawnStep / 2, SpawnStep / 2), -10);
            var gost = Instantiate(ghostprefab, p, Quaternion.identity, transform);
            ghostList.Add(gost);
        }

        interfaceManager.UpdateUserData(0, 0);

        inputManager.gameObject.SetActive(true);
    }

    private void InputManager_OnGhostClickedevent(GameObject ghost)
    {
        ghostList.Remove(ghost);
        Destroy(ghost);
        score += ScoreForGhost;
        interfaceManager.UpdateUserData(score, 10 - ghostList.Count);
    }

    private void Update() { }
}