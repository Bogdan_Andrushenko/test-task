﻿using UnityEngine;

public class InputManager : MonoBehaviour
{


    public delegate void OnGhostClicked(GameObject ghost);

    public event OnGhostClicked OnGhostClickedevent;

    void Update()
    {
#if UNITY_EDITOR


        if (Input.GetMouseButtonDown(0))
        {

            RaycastHit hit;
            var vektor = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
            Ray ray = Camera.main.ScreenPointToRay(vektor);
            if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Ghost")
            {
                OnGhostClickedevent.Invoke(hit.transform.gameObject);
            }
        }

#else

        if (Input.touchCount > 0)
        {
           
            UnityEngine.RaycastHit hit;
            var vektor = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0f);         
            Ray ray = Camera.main.ScreenPointToRay(vektor);

            if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Ghost")
            {               
               OnGhostClickedevent.Invoke(hit.transform.gameObject);
            }
        }
# endif
    }
}
